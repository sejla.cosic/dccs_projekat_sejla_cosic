<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.time.LocalDateTime" %>
<!DOCTYPE html>
<html>
  <head>
  	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
  	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="home.css">
  	<title>Email Simulator</title>
  </head>
  <body>
  	<br>
  	<nav id="navigation">
    	<button id="newButton" onclick="newMessageForm()">
    		<i class="material-icons" style="color: orange;float:left;">add_circle_outline</i>
		<span style="float:left;">New |</span>
    	</button>
    	<button onclick="deleteEmail()">
    		<i class="material-icons" style="color: orange;float:left;">delete</i>
		<span style="float:left;">Delete |</span>
    	</button>
    	<button onclick="archiveEmail()">
    		<i class="fa fa-archive" style="color: orange;font-size:20px;margin-right:5px;float:left;"></i>
		<span style="float:left;">Archive |</span>
    	</button>
    </nav>
    <br>
    <br>
    <span id="searchSection">
    	<span id="titleSection">
    		<h1 id="title">Posteingang</h1>
    	</span>
    	<br><br><br><br><br>
    	<nav id="emailNav">
    		<ul id="emailList">
    		</ul>
    	</nav>
   </span>
   <span id="content" >
   </span>
  
   <%@ include file="listMessages.jsp"%>	
   <script src="newForm.js"></script>
   <script src="showMessage.js"></script> 
   <script src="delete.js"></script> 
   <script src="archive.js"></script> 


  </body>
</html>
