<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ page import="entities.*" %>
<%@ page import="webProject.*" %>
<%
	
	ArrayList<Email> emailList = DataBase.getAllEmailsFromUser((String)session.getAttribute("user"));
	if(emailList != null){
		int n = emailList.size();
		for(int i=0;i<n;i++){
			Email e = (Email)emailList.get(i);
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
			
%>
			<script>
				var newButton = document.createElement("BUTTON");
				newButton.setAttribute("onclick","showMessage(this)");
				var str = document.createElement("STRONG");
				var newHeader = document.createElement("SPAN");
				newHeader.setAttribute("class","newHeader");
				var h = document.createTextNode(<%=e.getHeader()%>);
				newHeader.appendChild(h);
				str.appendChild(newHeader);
				newButton.appendChild(str);
				var br1 = document.createElement("BR");
				newButton.appendChild(br1);
				var newName = document.createElement("SPAN");
				newName.setAttribute("class","newEmailFrom");
				var n = document.createTextNode(<%=e.getFrom()%>);
				newName.appendChild(n);
				newButton.appendChild(newName);
				var left = document.createElement("SPAN");
				var l = document.createTextNode("<Cc ");
				left.appendChild(l);
				newButton.appendChild(left);
				var emailFrom = document.createElement("SPAN");
				emailFrom.setAttribute("class","Cc");
				var e = document.createTextNode(<%=e.getCc()%>);
				emailFrom.appendChild(e);
				newButton.appendChild(emailFrom);
				var right = document.createElement("SPAN");
				var r = document.createTextNode(">");
				right.appendChild(r);
				newButton.appendChild(right);
				var br2 = document.createElement("BR");
				newButton.appendChild(br2);
				var date = document.createElement("TIME");
				date.setAttribute("class","newDate");
				var d = document.createTextNode(<%=dateFormat.format(e.getDate())%>);
				date.appendChild(d);
				newButton.appendChild(date);
				var br3 = document.createElement("BR");
				newButton.appendChild(br3);
				var emailTo = document.createElement("SPAN");
				emailTo.setAttribute("class","newEmailTo");
				var e1 = document.createTextNode(<%=session.getAttribute("user")%>);
				emailTo.appendChild(e1);
				newButton.appendChild(emailTo);
				var content = document.createElement("P");
				content.setAttribute("class","newContent");
				var c = document.createTextNode(<%=e.getContent()%>);
				content.appendChild(c);
				newButton.appendChild(content);
				var newLi = document.createElement("LI");
				newLi.appendChild(newButton);
				document.getElementById("emailList").appendChild(newLi);
			</script>
<%
	
		}
	}
%>
