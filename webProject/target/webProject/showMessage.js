function showMessage(object){
  var newStyle = 'h1{margin-left:3%;text-align:left;}p,a{margin-left:3%;float:left;}#timeRecv{color: rgb(96, 96, 96);float:left;}#receiver{float:left;}';
  
  var newContent = document.getElementById("content");
  newContent.innerHTML = "";
  newContent.insertAdjacentHTML("afterbegin",'<h1 id="emailHeader"></h1><p><strong><span id="senderEmail"></span></strong>&lt;Cc<span id="Cc"></span>&gt;<br><small><time id="timeRecv"></time><br></small><small id="receiver"></small></p><p id="messageContent"></p>');

  var styleSheet = document.createElement("STYLE")
  styleSheet.type = "text/css"
  styleSheet.innerText = newStyle;
  newContent.appendChild(styleSheet);
  
  var header = object.querySelector('.newHeader').innerHTML;
  var Cc = object.querySelector('.Cc').innerHTML;
  var from = object.querySelector('.newEmailFrom').innerHTML;
  var date = object.querySelector('.newDate').innerHTML;
  var content = object.querySelector('.newContent').innerHTML;
  var to = object.querySelector('.newEmailTo').innerHTML;
  
  document.getElementById("emailHeader").innerHTML = header;
  document.getElementById("Cc").innerHTML = Cc;
  document.getElementById("senderEmail").innerHTML = from;
  document.getElementById("timeRecv").innerHTML = date;
  document.getElementById("receiver").innerHTML = to;
  document.getElementById("messageContent").innerHTML = content;
  
  var values = {"from":from,"cc":Cc,"header":header,"content":content,"date":date};
  var jsonStr = JSON.stringify(values);
  
  xhttp.open("POST", "addToDelete.jsp", true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send(jsonStr);
}
