<%@ page import="webProject.*" %>
<%@ page import="java.util.List" %>

<%
	String to = (String)request.getParameter("To");
	String cc = (String)request.getParameter("Cc");
	String from = (String)session.getAttribute("user");
	String header = (String)request.getParameter("subject");
	String content = (String)request.getParameter("messageBody");
	
	java.util.Date date = new java.util.Date();
	
	if(DataBase.createEmail(from,cc,header,content,date)){
		if(DataBase.addEmailToUser(to,from,cc,header,content,date)){
			response.sendRedirect("home.jsp");
		}
	}
	else{
		out.println("Email not sent, <a href='home.jsp'>try again</a>");
	}
%>