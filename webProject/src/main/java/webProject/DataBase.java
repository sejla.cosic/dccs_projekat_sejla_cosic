package webProject;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import entities.Archive;
import entities.Email;
import entities.User;

public class DataBase {
	static EntityManager em;
	
	public static Email getEmail(String from,String cc,String header,String content,java.util.Date date) {
		
		try {
			em=Connection.getEntityManager();
			em.getTransaction().begin();
		
			Query q = em.createNamedQuery("getEmail");
			q.setParameter("from", from);
			q.setParameter("cc", cc);
			q.setParameter("header", header);
			q.setParameter("content", content);
			q.setParameter("date", date);
		
			Email e = (Email)q.getSingleResult();
		
			em.getTransaction().commit();
			em.close();
		
			return e;	
		}catch(Exception e) {
			return null;
		}
	}
	
	public static User getUser(String username) {
		try {
			em=Connection.getEntityManager();
			em.getTransaction().begin();
		
			Query q = em.createNamedQuery("getUser");
			q.setParameter("username", username);
		
			User u = (User)q.getSingleResult();
		
			em.getTransaction().commit();
			em.close();
		
			return u;
		}
		catch(Exception e) {
			return null;
		}
	}
	
	public static ArrayList<Email> getAllEmailsFromUser(String username) {
		try {
			em=Connection.getEntityManager();
			em.getTransaction().begin();
		
			Query q = em.createNamedQuery("allEmailsFromUser");
			q.setParameter("username", username);
			List l = q.getResultList();
			ArrayList<Email> al = new ArrayList<Email>();
			for(int i=0;i<l.size();i++) {
				al.add((Email) l.get(i));
			}
			em.getTransaction().commit();
			em.close();
			
			return al;
		}
		catch(Exception e) {
			return null;
		}
	}
	
	public static boolean createEmail(String from,String cc,String header,String content,java.util.Date date) {
		try {
			em=Connection.getEntityManager();
			em.getTransaction().begin();

			Email newEmail = new Email(from,cc,header,content,date);
			em.persist(newEmail);
		
			em.getTransaction().commit();
			em.close();
			return true;
		}catch(Exception e){
			return false;
		}
	}
	
	public static boolean createUser(String username) {
		try{
			em=Connection.getEntityManager();
			em.getTransaction().begin();

			User newUser = new User(username);
			em.persist(newUser);
		
			em.getTransaction().commit();
			em.close();
			
			return true;
		}catch(Exception e) {
			return false;
		}
	}
	
	public static boolean addEmailToUser(String username,String from,String cc,String header,String content,java.util.Date date) {
		try {
			Email email = DataBase.getEmail(from,cc,header,content, date);
			User user = DataBase.getUser(username);
		
			em=Connection.getEntityManager();
			em.getTransaction().begin();
		
			user.getEmailCollection().add(email);
			email.setUser(user);
		
		
			em.getTransaction().commit();
			em.close();
			return true;
		}catch(Exception e) {
			return false;
		}
	}
	
	public static boolean deleteEmail(Email email,String username) {
		try {
			User user = DataBase.getUser(username);
			em=Connection.getEntityManager();
			em.getTransaction().begin();
			
			User user0 = em.merge(user);
	        for(Email e: user0.getEmailCollection()) {
	        	e.setUser(null);
	        }
	        
	        user0.getEmailCollection().clear();
	        
	        em.getTransaction().commit();
	     
	        Email email0 = em.merge(email);

	        em.remove(email0);
	        em.getTransaction().commit();
	        em.close();
	        return true;
		}
		catch(Exception e) {
			return false;
		}
	}
	
	public static boolean addToArchive(Email email,String username) {
		try {
			User user = DataBase.getUser(username);
			em=Connection.getEntityManager();
			em.getTransaction().begin();
			
			User user0 = em.merge(user);
	        for(Email e: user0.getEmailCollection()) {
	        	e.setUser(null);
	        }
	        
	        user0.getEmailCollection().clear();
	        em.getTransaction().commit();
	        
	        em.getTransaction().begin();

			Archive newArchive = new Archive(email.getEmailID(),user.getUserID());
			em.persist(newArchive);
		
			em.getTransaction().commit();
			em.close();
			return true;
		}catch(Exception e) {
			return false; 
		}
	}
	
	

}
