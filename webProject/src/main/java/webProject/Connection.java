package webProject;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public abstract class Connection {
	
	private Connection() {
		
	}
	
	private static EntityManagerFactory entityManagerFactory;
	
	public static EntityManager getEntityManager(){
		
		if (entityManagerFactory == null){
			entityManagerFactory = Persistence.createEntityManagerFactory("hsqldbConnection");
		}
		return entityManagerFactory.createEntityManager();
	}

}