package entities;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name="EMAIL")
@NamedQueries({
	@NamedQuery(name="allEmailsFromUser", query="SELECT email FROM User user ,Email email WHERE user.username=:username"),
	@NamedQuery(name="getEmail", query="SELECT email FROM Email email WHERE email.from=:from AND email.cc=:cc AND email.header=:header AND email.content=:content AND email.date=:date")
})
public class Email implements Serializable {

	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="EMAIL_ID")
	private int emailID;
	@Column(name="EMAIL_FROM")
	private String from;
	@Column(name="EMAIL_CC")
	private String cc;
	@Column(name="CONTENT")
	private String content;
	@Column(name="HEADER")
	private String header;
	@Column(name="RECEIVE_TIME")
	@Temporal(TemporalType.TIMESTAMP)
	private java.util.Date date;
	private User user;

	public Email() {
		super();
	}
	
	public Email(String from,String cc, String header,String content,java.util.Date date) {
		this.from = from;
		this.cc = cc;
		this.header = header;
		this.content = content;
		this.date = date;
	}
	
	public int getEmailID() {
		return this.emailID;
	}
	
	public String getFrom() {
		return this.from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getContent() {
		return this.content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
	public java.util.Date getDate(){
		return this.date;
	}
	
	public void setDate(java.util.Date date) {
		this.date = date;
	} 
	
	@ManyToOne()
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public String getCc() {
		return cc;
	}

	public void setCc(String cc) {
		this.cc = cc;
	}
   
   
}
