package entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: User
 *
 */
@Entity
@Table(name="USER")
@NamedQueries({
	@NamedQuery(name="getUser", query="SELECT user FROM User user WHERE user.username=:username"),
	@NamedQuery(name="getUserFromEmail", query="SELECT user FROM User user,Email email WHERE user.emailCollection = email AND email.emailID=:emailID")
})
	
	
public class User implements Serializable {

	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="USER_ID")
	private int userID;
	@Column(name="USERNAME")
    private String username;
	private Collection<Email> emailCollection = new ArrayList<Email>();
	
	public User() {
		super();
	}
	
	public User(String username) {
		this.username = username;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getUserID() {
		return userID;
	}

	@OneToMany(cascade = {CascadeType.REFRESH, CascadeType.DETACH}, mappedBy="user")
	public Collection<Email> getEmailCollection() {
		return emailCollection;
	}

	public void setEmailCollection(Collection<Email> emailCollection) {
		this.emailCollection = emailCollection;
	}
   
}
