package entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Archive
 *
 */
@Entity
@Table(name="ARCHIVE")
public class Archive implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="ARCHIVE_ID")
	private int archiveID;
	@Column(name="EMAIL_ID")
	private int emailID;
	@Column(name="USER_ID")
	private int userID;

	public Archive() {
		super();
	}
	
	public Archive(int emailID,int userID) {
		this.emailID = emailID;
		this.userID = userID;
	}

	public int getEmailID() {
		return emailID;
	}

	public void setEmailID(int emailID) {
		this.emailID = emailID;
	}

	public int getUserID() {
		return userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

	public int getArchiveID() {
		return archiveID;
	}
   
}
