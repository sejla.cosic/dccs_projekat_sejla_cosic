<%@ page import="org.json.JSONArray" %>
<%@ page import="org.json.JSONObject" %>
<%@ page import="java.text.*" %>
<%@ page import="entities.*" %>
<%@ page import="webProject.*" %>


<%
	String json = (String)session.getAttribute("delete");
	String user = (String)session.getAttribute("user");
	if(json != null){
		JSONObject obj = new JSONObject(json);
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		Email email = getEmail(obj.getString("from"),obj.getString("cc"),obj.getString("header"),obj.getString("content"),dateFormat.parse(obj.getString("date"))); 
		if(email != null){
			if(!DataBase.addToArchive(email,user)){
%>
				<script>
					alert("Error , email not archived.");
				</script>
<%
			}
			else
				response.sendRedirect("home.jsp");
		}
		else{
%>
			<script>
					alert("Error , email not archived.");
			</script>
<%
	   }
	}
%>